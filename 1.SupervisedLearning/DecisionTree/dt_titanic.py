import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeRegressor
from sklearn.tree import DecisionTreeClassifier
import matplotlib.pyplot as plt
from sklearn.model_selection import cross_val_score
import os 


print(os.getcwd())
##数据集下载  https://www.kaggle.com/competitions/titanic
data = pd.read_csv('./datasets/titanic/train.csv')
# print(data.head(5))
# print(data.info())
# print(data.columns)
 
##########数据预处理###############
#删除特征
data.drop(['Name','Ticket','Cabin'],inplace=True,axis=1)  #axis=1  对列进行操作
# print(data1.shape())
#处理缺失值
data['Age'] = data['Age'].fillna(data['Age'].mean())
data = data.dropna(axis=0)  #axis=0行 axis=1 列
#Embarked列转换为012
labels = data['Embarked'].unique().tolist()
data['Embarked'] = data['Embarked'].apply(lambda x: labels.index(x))
#性别转化为01，有两种方式
#法一
# labels1 = data['Sex'].unique().tolist()
# data['Sex'] = data['Sex'].apply(lambda x: labels1.index(x))
#法二
data['Sex'] = (data['Sex'] == 'male').astype('int')
data.loc[:,'Sex'] = (data['Sex'] == 'male').astype('int') #更推荐使用 loc里面必须写的是列名，而不能写数字，如果要写数字就用iloc
# print(data.head(5))
 
 
x = data.loc[:,data.columns != 'Survived']
y = data.loc[:,data.columns == 'Survived']
print(x.shape)
print(y.shape)
xtrain,xtest,ytrain,ytest = train_test_split(x,y,test_size=0.3)
#重整理索引 按从0开始
for i in [xtrain,xtest,ytrain,ytest]:
    i.index = range(i.shape[0])
# print(xtrain.index)
 
######模型#########
clf = DecisionTreeClassifier(random_state=25)
clf = clf.fit(xtrain,ytrain)
result = clf.score(xtest,ytest)
print(result)
 
clf1 = DecisionTreeClassifier(random_state=25)
score = cross_val_score(clf1,x,y,cv=5).mean()
print(score)
tr=[]
te=[]
for i in range(10):
    clf = DecisionTreeClassifier(random_state=25
                                 ,max_depth=i+1
                                 ,criterion='entropy')
    clf = clf.fit(xtrain,ytrain)
    score_tr = clf.score(xtrain,ytrain)
    score_te = cross_val_score(clf,x,y,cv=5).mean()
    tr.append(score_tr)
    te.append(score_te)
print('最高分数',max(te))
 
plt.plot(range(1,11),tr,color='red',label='train')
plt.plot(range(1,11),te,color='green',label='test')
plt.legend()
plt.show()
 
###########网格搜索###########################
'''
能够帮助我们同时调整多个参数的技术，枚举技术
缺点：因为是枚举，一个一个试，所以计算量非常大，耗时，并且其实网格搜索可能没有自己设置的高，因为设置的都不能舍弃
'''
from sklearn.model_selection import GridSearchCV
import numpy as np
 
gini_threholds = np.linspace(0,0.5,10)#随机的有顺序的五十个数
# entropy_threholds = np.linspace(0,1,50)#随机的有顺序的五十个数
# np.arange(0,0.5,0.01)#随机的步长为0.01的数
#一连串的参数和参数对应的我们希望网格搜索的参数的取值范围
parameters = {'criterion':('gini','entropy')
              ,'splitter':('best','random')
              ,'max_depth':[*range(1,10)]
              ,'min_samples_leaf':[*range(1,50,5)]
              ,'min_impurity_decrease':gini_threholds
              }
#实例化模型
clf = DecisionTreeClassifier(random_state=42)
GS = GridSearchCV(clf,parameters,cv=5)
GS = GS.fit(xtrain,ytrain)
# s = GS.score(xtest,ytest)
 
print('最优组合',GS.best_params_)
print('最优分数',GS.best_score_)