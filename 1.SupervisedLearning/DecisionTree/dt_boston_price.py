from sklearn.datasets import load_boston
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
import numpy as np
import matplotlib.pyplot as plt
 
boston = load_boston()
# print(boston.data.shape)
regressor = DecisionTreeRegressor(random_state=42)  #实例化
xtrain,xtest,ytrain,ytest = train_test_split(boston.data,boston.target,test_size=0.3)
regressor.fit(xtrain,ytrain)
score = regressor.score(xtest,ytest)
print(score)
# 交叉验证
cross = cross_val_score(regressor,boston.data,boston.target,cv=10,scoring='neg_mean_squared_error')  #负均方误差
cross1 = cross_val_score(regressor,boston.data,boston.target,cv=10)  #  以R的平方进行评估，越接近1越好
print(cross)
print(cross1)