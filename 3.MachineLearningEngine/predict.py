## 模型部署到云端

from flask import Flask, request, jsonify
import joblib
import pandas as pd

app = Flask(__name__)

@app.route("/titannic", methods=["POST"])  # 请求方法为 POST
def predict():
    json_ = request.json  # 解析请求数据
    query_df = pd.DataFrame(json_)  # 将 JSON 变为 DataFrame
    columns_onehot = [
        "pclass",
        "sex_female",
        "sex_male",
        "embarked_C",
        "embarked_Q",
        "embarked_S",
    ]  # 独热编码 DataFrame 列名
    query = pd.get_dummies(query_df).reindex(
        columns=columns_onehot, fill_value=0
    )  # 将请求数据 DataFrame 处理成独热编码样式
    clf = joblib.load("../model/titanic.pkl")  # 加载模型
    prediction = clf.predict(query)  # 模型推理
    print(f"prediction = {prediction}")
    prediction = [str(pred) for pred in prediction]
    return jsonify({"prediction": list(prediction)})  # 返回推理结果

@app.route("/mushroom", methods=["POST"])  # 请求方法为 POST
def inference():
    query_df = pd.DataFrame(request.json)  # 将 JSON 变为 DataFrame
    
    df = pd.read_csv("../datasets/mushrooms.csv")  # 读取数据
    X = pd.get_dummies(df.iloc[:, 1:])  # 读取特征并独热编码
    query = pd.get_dummies(query_df).reindex(columns=X.columns, fill_value=0)  # 将请求数据 DataFrame 处理成独热编码样式
    
    clf = joblib.load('../model/mushrooms.pkl')  # 加载模型
    prediction = clf.predict(query)  # 模型推理
    return jsonify({"prediction": list(prediction)})  # 返回推理结果


if __name__ == '__main__':
    app.run(debug=True)